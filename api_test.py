import ffmpeg
import requests

import os, shutil, sys, time
import threading
from datetime import timedelta

DIR = "./tmp"
API_URL = "http://127.0.0.1:5000/resize"


def thread(index, delay, dir, precache_dir, width, height):
	time.sleep(delay)

	timestamp = time.time()
	files_list = os.listdir(precache_dir)
	for frame_filename in files_list:
		frame_filepath = os.path.join(precache_dir, frame_filename)
		result_filepath = os.path.join(dir, frame_filename)
		response = call_api(frame_filepath, frame_filename, width, height)
		store_response(response, result_filepath)

	benchmark = time.time() - timestamp
	print('Instance %s took me %s seconds, it was around %s FPS' % (index, round(benchmark*1000)/1000, round(len(files_list)/benchmark)))


def call_api(frame_filepath, frame_filename, width, height):
	with open(frame_filepath, 'rb') as frame_file:
		files = {'file': (frame_filename, frame_file, 'image/jpeg')}
		data = {'width': width, 'height': height}
		r = requests.post(API_URL, files=files, data=data, stream=True)
		r.raise_for_status()
	return r


def store_response(r, result_filepath):
	with open(result_filepath, 'wb') as file_handler:
		for block in r.iter_content(1024):
			file_handler.write(block)


def run():
	try:
		offset = int(sys.argv[1])
		instances = int(sys.argv[2])
		width = int(sys.argv[3])
		height = int(sys.argv[4])
		file = sys.argv[5]
	except:
		print("Usage:")
		print("    python api_test.py <M> <N> <W> <H> <filename>")
		print()
		print("        M:      time offset for every next instance in seconds")
		print("        N:      number of instances to test")
		print("        W:      width for precessed frames")
		print("        H:      height for precessed frames")
		return -1

	if not os.path.isfile(file):
		print("%s doesn't look like a file to me" % file)
		return -2

	precache_dir = os.path.join(DIR, file)
	if os.path.exists(precache_dir):
		shutil.rmtree(precache_dir)
	os.mkdir(precache_dir)
	timestamp = time.time()
	print("Splitting video to frames via FFmpeg... ", end="", flush=True)
	( ffmpeg
		.input(file)
		.output(os.path.join(precache_dir, "frame_%03d.jpeg"))
		.run(quiet=True)
	)
	print("Done! %s frames total" % len(os.listdir(precache_dir)))

	delay = 0
	threads = []
	for i in range(instances):
		dir = precache_dir + ('_instance-%s' % i)
		if os.path.exists(dir):
			shutil.rmtree(dir)
		os.mkdir(dir)
		t = threading.Thread(target=thread, args=(i, delay, dir, precache_dir, width, height))
		threads.append(t)
		t.start()
		delay += offset


if __name__ == "__main__":
	run()
