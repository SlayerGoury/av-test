Tested with Python 3.6.7 in virtual environment with everything from requirements.txt installed.

Even though this doesn't seem to be in the scope of this task, but because I was asked to make it use more resources and also serve concurrent requests, here is one way run this thing:

  gunicorn -w <cores> -b <address:port> wsgi:app

Make sure to use the same address and port as defined in api_test.py
