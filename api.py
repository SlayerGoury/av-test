from flask import Flask, request, send_file
from io import BytesIO
from PIL import Image

app = Flask(__name__)


@app.route("/", methods=['GET'])
def index():
	return "index placeholder"


@app.route("/resize", methods=['POST'])
def resize():
	if 'file' not in request.files:
		return "file is required", 400
	file = request.files['file']
	image = Image.open(file)
	width = int(request.form['width'])
	height = int(request.form['height'])
	return return_image( image.resize((width,height)) )


def return_image(pillow_image):
	io = BytesIO()
	pillow_image.save(io, 'JPEG', quality=80)
	io.seek(0)
	return send_file(io, mimetype='image/jpeg')
